import { spawnImpl } from "./spawnImpl";

export function spawnIt(cmd: string, parameters: string[] = [], options: any = {}): Promise<string> {
  options.stdio = ["ignore", "pipe", "inherit"];
  return new Promise((resolve, reject) => {
    const spawned = spawnImpl(cmd, parameters, options);
    let output = "";
    spawned.stdout.on("data", (data) => { output += data.toString(); });
    spawned.on("exit", exitCode => {
      if (!options.silent) {
        console.log(output);
      }
      if (exitCode === 0) {
        resolve(output);
      } else {
        reject(new Error(`Error while spawning: ${cmd} ${parameters.join(" ")}`));
      }
    });
  });
}
