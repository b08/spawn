import { spawnImpl } from "./spawnImpl";

export async function spawn(cmd: string, ...parameters: string[]): Promise<void> {
  return new Promise((resolve, reject) => {
    const spawned = spawnImpl(cmd, parameters, { stdio: "inherit" });
    spawned.on("exit", exitCode => {
      if (exitCode === 0) {
        resolve();
      } else {
        reject(new Error(`Error while spawning: ${cmd} ${parameters.join(" ")}`));
      }
    });
  });
}
