import * as childProcess from "child_process";

export function spawnImpl(cmd: string, parameters: string[], options: any): childProcess.ChildProcess {
  options.shell = process.platform === "win32" ? process.env.ComSpec : "/bin/sh";
  return childProcess.spawn(cmd, parameters, options);
}
