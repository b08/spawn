# @b08/spawn, seeded from @b08/library-seed, library type: support
Method to spawn process to work on both linux and windows. \
Works with recommended shell parameter. \
Second function is to capture process output.

# usage
spawn is just cross-platform (Linux/Windows) spawn converted to promise. Stdio is set to "inherit"
```
import { spawn } from "@b08/spawn";

async function compileTypescript(): Promise<void> {
  await spawn("npx", "tsc");
}
```


spawnIt command captures process output, allows to set up process options, except stdio, which is set to {"ignore", "pipe", "inherit"}

```
import { spawnIt } from "@b08/spawn";

async function collectLocTask(): Promise<void> {
  const locOutput = await spawnIt("npx", ["sloc", "./src"]);
  const regex = /\s*Source\s*:\s*(\d+)\s*/;
  const match = locOutput.match(regex);
  const loc = +match[1];
  // do something with collected Lines Of Code metrics
}
```

If error happens in the spawned process (exit code is not 0), it prints out collected output to console and throws error.