import { test } from "@b08/test-runner";
import { spawnIt, spawn } from "../src";

test("spawnIt should return the process output", async expect => {
  // arrange

  // act
  const result = await spawnIt("echo", ["1"], { silent: true });

  // assert
  expect.equal(result.replace(/[\r\n]+/g, ""), "1");
});

test("spawn should not throw", async () => {
  // arrange

  // act
  await spawn("echo", "1");

  // assert
  // visually control output
});
